FROM scratch
MAINTAINER Joe Julian <me@joejulian.name>

#ADD archlinux-bootstrap-2017.08.01-x86_64.tar.gz /
ADD root.x86_64/ /
COPY mirrorlist /etc/pacman.d/

RUN pacman-key --init && \
    pacman-key --populate && \
    pacman -Syu --noconfirm && \
    pacman -Scc --noconfirm
