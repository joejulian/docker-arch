#!/bin/sh
set -eu

echo "running: build.sh $*"

DAY_OF_MONTH=$(wget -qO- https://mirrors.kernel.org/archlinux/iso/ | grep '[0-9.]\{10\}' | sed 's@.*href="\([^/]\+\)/".*@\1@' | sort -d | tail -n1)
BOOTSTRAP="archlinux-bootstrap-${DAY_OF_MONTH}-x86_64.tar.zst"
URL=https://mirrors.kernel.org/archlinux/iso/${DAY_OF_MONTH}/${BOOTSTRAP}
DATETAG="archlinux-${DAY_OF_MONTH}"
SHA=$(echo "$2" | sed 's/ //')

echo "Installing a tar version that's capable of extracting zstd"
apk update
apk add tar zstd

echo "Unpacking ${URL}"
wget -qO- "${URL}" | tar -x --zstd

docker build . -t "$1"
if [[ -z ${3+x} ]]; then
    docker tag "$1" "${CI_REGISTRY_IMAGE}:${DATETAG}"
    docker tag "$1" "${CI_REGISTRY_IMAGE}:latest"
    echo docker tag "$1" "${QUAY_REGISTRY_IMAGE}:${SHA}"
    docker tag "$1" "${QUAY_REGISTRY_IMAGE}:${SHA}"
    docker tag "$1" "${QUAY_REGISTRY_IMAGE}:${DATETAG}"
    docker tag "$1" "${QUAY_REGISTRY_IMAGE}:latest"
    docker push "$1"
    docker push "${CI_REGISTRY_IMAGE}:${DATETAG}"
    docker push "${CI_REGISTRY_IMAGE}:latest"
    docker push "${QUAY_REGISTRY_IMAGE}:${SHA}"
    docker push "${QUAY_REGISTRY_IMAGE}:${DATETAG}"
    docker push "${QUAY_REGISTRY_IMAGE}:latest"
else
    echo "Skipping publish"
fi
